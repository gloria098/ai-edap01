import gym
import random
import requests
import numpy as np
import argparse
import sys
import copy
from gym_connect_four import ConnectFourEnv

env: ConnectFourEnv = gym.make("ConnectFour-v0")

#SERVER_ADRESS = "http://localhost:8000/"
SERVER_ADRESS = "https://vilde.cs.lth.se/edap01-4inarow/"
API_KEY = 'nyckel'
STIL_ID = ["gl6645mo-s"] # TODO: fill this list with your stil-id's

def call_server(move):
   res = requests.post(SERVER_ADRESS + "move",
                       data={
                           "stil_id": STIL_ID,
                           "move": move, # -1 signals the system to start a new game. any running game is counted as a loss
                           "api_key": API_KEY,
                       })
   # For safety some respose checking is done here
   if res.status_code != 200:
      print("Server gave a bad response, error code={}".format(res.status_code))
      exit()
   if not res.json()['status']:
      print("Server returned a bad status. Return message: ")
      print(res.json()['msg'])
      exit()
   return res

def check_stats():
   res = requests.post(SERVER_ADRESS + "stats",
                       data={
                           "stil_id": STIL_ID,
                           "api_key": API_KEY,
                       })

   stats = res.json()
   return stats

"""
You can make your code work against this simple random agent
before playing against the server.
It returns a move 0-6 or -1 if it could not make a move.
To check your code for better performance, change this code to
use your own algorithm for selecting actions too
"""
def opponents_move(env):
   env.change_player() # change to oppoent
   avmoves = env.available_moves()
   if not avmoves:
      env.change_player() # change back to student before returning
      return -1

   # TODO: Optional? change this to select actions with your policy too
   # that way you get way more interesting games, and you can see if starting
   # is enough to guarrantee a win
   action = random.choice(list(avmoves))

   state, reward, done, _ = env.step(action)
   if done:
      if reward == 1: # reward is always in current players view
         reward = -1
   env.change_player() # change back to student before returning
   return state, reward, done

def score_positions(window, piece): #evaluate window
	score = 0
	opp_piece = 1
	if piece == 1:
		opp_piece = -1

	if window.count(piece) == 4: #räkna hur många av piece som finns i window
		score += 10000
	elif window.count(piece) == 3 and window.count(0) == 1:
		score += 1000
	elif window.count(piece) == 2 and window.count(0) == 2:
		score += 100

	if window.count(opp_piece) == 3 and window.count(0) == 1:
		score -= 1000

	return score


def eval(env, piece): #score_position
    score =0
    # Test rows
    for i in range(env.board_shape[0]): #vertikalt 6 st. 0 1 2 3 4 5. en loop från siffran 0-5.
        row_array = [int(r) for r in list(env.board[i, :])] #env.board är en matris [[]]. env.board[i, :] = gå igenom rad i och alla kolumner i matrisen env.board. plocka ut den raden och lägg den i en lista  [[]]
        #print(row_array)
        for j in range(env.board_shape[1] - 3): #0 1 2 3. en loop från 0-3.
            window = row_array[j:j + 4] #gå igenom vektorn row array för varje rad och kolla 4 bitar åt gången
            score = score + score_positions(window,piece) #poängsätt 4 bitar åt gången baserat på hur många som finns i rad.


    # Test columns on transpose array
    reversed_board = np.array([list(i) for i in zip(*env.board)])
    env.reset()
    for i in range(env.board_shape[1]): #horisontellt 7 st. 0 1 2 3 4 5 6. en loop från siffran 0-6.
        col_array = [r for r in list(reversed_board[i, :])] #convert numpy array(matris[[]]) to list[]. gå igenom row i, och alla columns i reversed_board matrisen. convertera till en lista[] och lägg i en till lista så det blir matris med en rad[[0 0 0 0 0 0 ]]
        for j in range(env.board_shape[0] - 3): #0 1 2. en for loop som går från 0-2.
            window = col_array[j:j + 4] #kolla vektorn/kolumnen som är col_array genom att kolla 4 brickor åt gången och poängsätt utifrån hur många brickor den har
            score = score + score_positions(window, piece)

    # Test diagonal
    for i in range(env.board_shape[0] - 3): #0 1 2. for loop som går från 0-2. styr vilken rad vi tittar på.
        for j in range(env.board_shape[1] - 3): #0 1 2 3. for loop som går från 0-3. styr vilken kolumn vi tittar på.
            window = [env.board[i+k][j+k] for k in range(4)] #ta matrisen env.board[[]]. kolla på rad i+k och kolumn j+k. k är en loop från siffran 0-3
            score = score + score_positions(window,piece)

    # Test reverse diagonal
    reversed_board = np.fliplr(env.board)
    for i in range(env.board_shape[0] - 3):
        for j in range(env.board_shape[1] - 3):
            window = [reversed_board[i + k][j + k] for k in range(4)]
            score = score + score_positions(window, piece)

    return score


#alla rekursiva anrop på minimax görs för noderna inuti ett och samma träd, för att avgöra vilken av de två noderna under som varje nod ska välja mellan.
#varje riktigt drag har ett eget träd och det är noden längst upp i det trädet som är det befintliga statet i brädet, dvs det senaste draget i trädet. alla noder uner är hypotetiska drag som kan göras 4 drag framåt. varje nytt riktigt drag innebär ett nytt träd med hypotetiska drag.
def minimax(env, depth, alpha, beta, maximizingPlayer): #env is node, variabel för att kalla funkt. depth håller inte reda på var vi är i trädet utan hur långt vi har kvar att gå. maxmplay är spelare som ska maximieras dvs skicka in 1 som inparameter.
  if depth==0 or len(env.available_moves())==0: #när det inte längre finns några moves kvar att göra. depth == 0 -> vi kollat 4 drag i framtiden, för varje drag så gör vi depth-1. vi börjar med att kolla djup 4
     return eval(env, 1) #här slutar koden efter alla rekursiva metoder så att vi är längst ut i grenen. detta är det poänget vi tilldelar draget längst ner

  if maximizingPlayer: #om det är vårt drag
      bestScore= float('-inf')
      for m in env.available_moves(): #gå igenom varje element i listan moves = [0 1 2 3 4 5 6]. listan är de tillgängliga movsen som finns kvr att göra
        node = copy.deepcopy(env)
        node.step(m) #gör steg m i denna noden
        score = minimax(node, depth -1, alpha, beta, False) #poängen för noden returneras
        if(score > bestScore): #noden ovan avgör vilken av de två undre dden ska välja
            bestScore = score
        if(score >= beta):
            break
        alpha = max(alpha, score)
      return bestScore

  else: #om det är motspelarens drag
      bestScore = float('+inf')
      for m in env.available_moves():
         node = copy.deepcopy(env)
         node.step(m) #step returnerar ett brädet i ett state. temp_state kan användas för att uppdatera det riktiga steget
         score = minimax(node,depth-1, alpha, beta, True)
         if (score < bestScore):
             bestScore = score
         if (score <= alpha):
             break
         beta= min(beta, score)

      return bestScore

def student_move(env,depth, alpha, beta): #kallas för varje faktsikt drag vi gör, varje kallelse på studentmove är ett nytt träd. snuttkoden här särskiljer det nästa draget som faktiskt ska göras (dvs noderna strax under moder noden) och de andra hypotetiska dragen vars vi räknat ut score för
    bestScore= float('-inf')
    moves = env.available_moves()
    bestMove = 0
    for m in moves:
        node = copy.deepcopy(env)
        node.step(m)
        score = minimax(node, depth - 1, alpha, beta, False) #minimax kallas för varje möjligt drag vi kan göra härnäst
        if(score > bestScore):
            bestScore = score
            bestMove = m
    return bestMove

"""
   TODO: Implement your min-max alpha-beta pruning algorithm here.
   Give it whatever input arguments you think are necessary
   (and change where it is called).
   The function should return a move from 0-6
"""

   #return random.choice([0, 1, 2, 3, 4, 5, 6])

def play_game(vs_server = False):
   """
   The reward for a game is as follows. You get a
   botaction = random.choice(list(avmoves)) reward from the
   server after each move, but it is 0 while the game is running
   loss = -1
   win = +1
   draw = +0.5
   error = -10 (you get this if you try to play in a full column)
   Currently the player always makes the first move
   """

   # default state
   state = np.zeros((6, 7), dtype=int)


   # setup new game
   if vs_server:
      env.reset()
      # Start a new game
      res = call_server(-1) # -1 signals the system to start a new game. any running game is counted as a loss

      # This should tell you if you or the bot starts
      print(res.json()['msg'])
      botmove = res.json()['botmove']
      state = np.array(res.json()['state'])
      env.reset(board=state) #uppdatera brädet i env till state
   else:
      # reset game to starting state
      env.reset(board=None)

      # determine first player
      student_gets_move = random.choice([True, False])
      if student_gets_move:
         print('You start!')
         print()
      else:
         print('Bot starts!')
         print()

   # Print current gamestate
   print("Current state (1 are student discs, -1 are servers, 0 is empty): ")
   print(state)
   print()

   done = False
   while not done:
      # Select your move

      stmove = student_move(env,4, float('-inf'), float('+inf')) # TODO: change input here

      # make both student and bot/server moves
      if vs_server:
         # Send your move to server and get response
         res = call_server(stmove)
         print(res.json()['msg'])

         # Extract response values
         result = res.json()['result']
         botmove = res.json()['botmove']
         state = np.array(res.json()['state'])
         env.reset(board=state)
      else:
         if student_gets_move:
            # Execute your move
            avmoves = env.available_moves()
            if stmove not in avmoves:
               print("You tied to make an illegal move! You have lost the game.")
               break
            state, result, done, _ = env.step(stmove)

         student_gets_move = True # student only skips move first turn if bot starts

         # print or render state here if you like

         # select and make a move for the opponent, returned reward from students view
         if not done:
            state, result, done = opponents_move(env)

      # Check if the game is over
      if result != 0:
         done = True
         if not vs_server:
            print("Game over. ", end="")
         if result == 1:
            print("You won!")
         elif result == 0.5:
            print("It's a draw!")
         elif result == -1:
            print("You lost!")
         elif result == -10:
            print("You made an illegal move and have lost!")
         else:
            print("Unexpected result result={}".format(result))
         if not vs_server:
            print("Final state (1 are student discs, -1 are servers, 0 is empty): ")
      else:
         print("Current state (1 are student discs, -1 are servers, 0 is empty): ")

      # Print current gamestate
      print(state)
      print()

def main():
   # Parse command line arguments
   parser = argparse.ArgumentParser()
   group = parser.add_mutually_exclusive_group()
   group.add_argument("-l", "--local", help = "Play locally", action="store_true")
   group.add_argument("-o", "--online", help = "Play online vs server", action="store_true")
   parser.add_argument("-s", "--stats", help = "Show your current online stats", action="store_true")
   args = parser.parse_args()

   # Print usage info if no arguments are given
   if len(sys.argv)==1:
      parser.print_help(sys.stderr)
      sys.exit(1)

   if args.local:
      play_game(vs_server = False)
   elif args.online:
      play_game(vs_server = True)

   if args.stats:
      stats = check_stats()
      print(stats)

   # TODO: Run program with "--online" when you are ready to play against the server
   # the results of your games there will be logged
   # you can check your stats bu running the program with "--stats"

if __name__ == "__main__":
    main()
